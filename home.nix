{ config, pkgs, ... }:
  let 
   # See https://github.com/mozilla/nixpkgs-mozilla
   moz_overlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
   nixpkgs-unstable = import <nixpkgs> {overlays = [moz_overlay]; }; 
   all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
  in
{
 # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
 
  # Packages
  home.packages = with nixpkgs-unstable; [ 
    clojure
    jdk
    leiningen
    xclip
    tree
    zlib
    cabal-install
    haskellPackages.hoogle
    haskellPackages.apply-refact
    haskellPackages.hlint
    haskellPackages.haddock
    ntfs3g
    ormolu
    clang 
    cmus
    ncmpcpp 
    nodejs-12_x 
    unrar
    stack 
    libGL
    #    latest.rustChannels.nightly.rust
    #    latest.rustChannels.nightly.rustup
    rustup
    xmobar 
    libnl 
    alacritty 
    dmenu 
    steam
    lutris 
    sbcl
    zeroad
    lispPackages.quicklisp
    lispPackages.quicklisp-to-nix
    lispPackages.swank
    haskellPackages.purescript
    (all-hies.selection 
      {selector = p: { inherit (p) ghc864 ghc865;}; }) ];
  
  # Vim
  programs.vim.enable = true;

  # MPD
  services.mpd.enable = true;

  # Neovim
  programs.neovim.enable = true;
  programs.neovim.package = nixpkgs-unstable.neovim;
  programs.neovim.withPython3 = true;
  programs.neovim.withPython = true;
  programs.neovim.plugins = [
    nixpkgs-unstable.vimPlugins.nerdtree
    nixpkgs-unstable.vimPlugins.nerdcommenter
    nixpkgs-unstable.vimPlugins.haskell-vim
    nixpkgs-unstable.vimPlugins.auto-pairs
    nixpkgs-unstable.vimPlugins.vim-fugitive
    nixpkgs-unstable.vimPlugins.denite-nvim
    nixpkgs-unstable.vimPlugins.denite-git
    nixpkgs-unstable.vimPlugins.coc-nvim
    nixpkgs-unstable.vimPlugins.coc-rls
    nixpkgs-unstable.vimPlugins.coc-json
    nixpkgs-unstable.vimPlugins.vim-airline
    nixpkgs-unstable.vimPlugins.vim-airline-themes
    nixpkgs-unstable.vimPlugins.awesome-vim-colorschemes
  ];
  programs.neovim.extraConfig = ''
    set nobackup
    set hidden
    set cmdheight=2
    set signcolumn=yes
    set number relativenumber
    set encoding=UTF-8
    set tabstop=2
    set shiftwidth=2
    set expandtab
    filetype plugin indent on
    set background=dark
    colorscheme gruvbox
    let g:airline_theme = 'gruvbox'
    let g:airline#extensions#tabline#enabled = 1
    let g:airline_powerline_fonts = 1
    syntax on

    map <C-n> :NERDTreeToggle<CR>
    nnoremap <C-f> :%!ormolu<CR>

    " COC 

    " Use tab for trigger completion with characters ahead and navigate.
    " Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
    inoremap <silent><expr> <TAB>
    \ pumvisible() ? "\<C-n>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ coc#refresh()
    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
    endfunction

    " Use <c-space> to trigger completion.
    inoremap <silent><expr> <c-space> coc#refresh()

    " Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
    " Coc only does snippet and additional edit on confirm.
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
    " Or use `complete_info` if your vim support it, like:
    " inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

    " Use `[g` and `]g` to navigate diagnostics
    nmap <silent> [g <Plug>(coc-diagnostic-prev)
    nmap <silent> ]g <Plug>(coc-diagnostic-next)

    " Remap keys for gotos
    nmap <silent> gd <Plug>(coc-definition)
    nmap <silent> gy <Plug>(coc-type-definition)
    nmap <silent> gi <Plug>(coc-implementation)
    nmap <silent> gr <Plug>(coc-references)

    " Use K to show documentation in preview window
    nnoremap <silent> K :call <SID>show_documentation()<CR>

    function! s:show_documentation()
    if (index(['vim','help'], &filetype) >= 0)
      execute 'h '.expand('<cword>')
    else
      call CocAction('doHover')
        endif
        endfunction

        " Highlight symbol under cursor on CursorHold
        autocmd CursorHold * silent call CocActionAsync('highlight')

        " Remap for rename current word
        nmap <leader>rn <Plug>(coc-rename)

        " Remap for format selected region
        xmap <leader>f  <Plug>(coc-format-selected)
        nmap <leader>f  <Plug>(coc-format-selected)

        augroup mygroup
        autocmd!
        " Setup formatexpr specified filetype(s).
        autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
        " Update signature help on jump placeholder
        autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
        augroup end

        " Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
        xmap <leader>a  <Plug>(coc-codeaction-selected)
        nmap <leader>a  <Plug>(coc-codeaction-selected)

        " Remap for do codeAction of current line
        nmap <leader>ac  <Plug>(coc-codeaction)
        " Fix autofix problem of current line
        nmap <leader>qf  <Plug>(coc-fix-current)

        " Create mappings for function text object, requires document symbols feature of languageserver.
        xmap if <Plug>(coc-funcobj-i)
        xmap af <Plug>(coc-funcobj-a)
        omap if <Plug>(coc-funcobj-i)
        omap af <Plug>(coc-funcobj-a)

        " Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
        nmap <silent> <C-d> <Plug>(coc-range-select)
        xmap <silent> <C-d> <Plug>(coc-range-select)

        " Use `:Format` to format current buffer
        command! -nargs=0 Format :call CocAction('format')

        " Use `:Fold` to fold current buffer
        command! -nargs=? Fold :call     CocAction('fold', <f-args>)

        " use `:OR` for organize import of current buffer
        command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')


  " Using CocList
    " Show all diagnostics
    nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
    " Manage extensions
    nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
    " Show commands
    nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
    " Find symbol of current document
    nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
    " Search workspace symbols
    nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
    " Do default action for next item.
    nnoremap <silent> <space>j  :<C-u>CocNext<CR>
    " Do default action for previous item.
    nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
    " Resume latest coc list
    nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

  " Ormolu on save
    autocmd FileType haskell autocmd BufWritePre <buffer> %!ormolu
  '';

  # Firefox
  programs.firefox.enable = true;
  
  # Alacritty 
  programs.alacritty.enable = true;
  programs.alacritty.settings = {
    window.padding = {
      x = 5;
      y = 5;
    };
    window.decorations = "full";
    window.title = "Alacritty";
    font.normal = {
      family = "Hack";
      style = "Regular";
    };
    font.size = 14.0;
    background_opacity = 0.8;
    shell.program = "${pkgs.zsh}/bin/zsh";
    selection.save_to_clipboard = true;
    live_config_reload = true;
    colors = {
      primary = {
        background = "0x282828";
        foreground = "0xebdbb2";
      };
      cursor = {
        text = "0xFF261E";
      	cursor = "0xFF261E";
      };
      normal = {
	      black = "0x282828";
	      red = "0xcc241d";
	      green = "0x98971a";
	      yellow = "0xd79921";
	      blue = "0x458588";
	      magenta = "0x66292f";
	      cyan = "0x689d6a";
	      white = "0xa89984";
      };
      bright = {
	      black = "0x928374";
        red = "0xfb4934";
        green = "0xb8bb26";
        yellow = "0xfabd2f";
        blue = "0x83a598";
        magenta = "0x8a4b53";
        cyan = "0x8ec07c";
        white = "0xebdbb2";
      };
    };
    dynamic_title = true;
  };

  # Zsh
  programs.zsh.enable = true;
  programs.zsh.oh-my-zsh = {
    enable = true;
    theme = "bureau";
  };

  # Xmonad 
  #xsession.enable = true;
  #xsession.windowManager.xmonad.enable = true;
  #xsession.windowManager.xmonad.enableContribAndExtras = true;
  #xsession.initExtra = ''
  #  autorandr -l home	
  #  feh --bg-scale /home/cptrodolfox/backgrounds/saturno-1.jpg
  #'';
 
  # System font
  gtk.enable = true;
  gtk.font = {
    name = "Hack Regular 16";
  };
  fonts.fontconfig.enable = true;

  # Autorandr
  programs.autorandr.enable = true;
  programs.autorandr.profiles = {
    "home" = {
      fingerprint = {
        HDMI-0 = "00ffffffffffff001c882190000000012f150103803c22610ad46ca357499c2511484b21080081800101010101010101010101010101011d007251d01e206e285500c48e21000018000000fc00434848574a540a202020202020000000fd001750185010000a202020202020000000000000000000000000000000000018014d02032375500102030405101112131415161f202122230907038301000065030c001000011d8018711c1620582c250045270000009ed60980a020e02d101060a200462700000018011d007251d01e206e28550046270000001e8c0ad08a20e02d10103e96004627000000188c0ad090204031200c4055004627000000180000f8";
      };
      config = {
	HDMI-0 = {
	  enable = true;
	  primary = true;
	  position = "0x0";
	  mode = "1920x1080";
	  rate = "59.94";
	};
      };
    };
  };

  # Compton
  #services.compton.enable = true;
  #services.compton.activeOpacity = "1.0";
  #services.compton.inactiveOpacity = "1.0";
  #services.compton.opacityRule = ["100:class_g = 'Firefox'" "100:class_g = 'Steam'" "80:class_g = 'Alacritty'"];

  # Feh
  programs.feh.enable = true;

  # Rofi
  programs.rofi.enable = true;
  programs.rofi.cycle = true;
  programs.rofi.font = "Droid Sans Mono 14";

  # Git
  programs.git.enable = true;
  programs.git.userEmail = "arellanowr@gmail.com";
  programs.git.userName = "wild_willy";

  # Taskwarrior
  programs.taskwarrior.enable = true;
  programs.taskwarrior.colorTheme = "solarized-dark-256";
  
  # Starship prompt
  programs.starship.enable = true;
  programs.starship.enableZshIntegration = true;

  # Emacs 
  programs.emacs.enable = true;
  services.emacs.enable = true;
  programs.emacs.extraPackages = epkgs : [
    epkgs.slime
    epkgs.nix-mode
    epkgs.auto-complete
    epkgs.auto-complete-pcmp
    epkgs.rainbow-delimiters
    epkgs.ac-slime
    epkgs.eval-sexp-fu
    epkgs.idle-highlight-mode
    epkgs.magit
    epkgs.smartparens
    epkgs.doom-themes
    epkgs.doom-modeline ];

  # Tmux 
  programs.tmux.enable = true;
  programs.tmux.extraConfig = ''
    set-option -g default-shell /home/cptrodolfox/.nix-profile/bin/zsh  			     
  '';

}
